variable "app_name" {
  description = "Application name that this grafana will monitor"
  type        = string
}

variable "cluster_name" {
  description = "ECS cluster to deploy the service"
  type        = string
}

variable "database_name" {
  description = "Name of the postgres database that grafana will use"
  type        = string
}

variable "desired_task_cpu" {
  description = "Task CPU Limit"
  default     = 1024
}

variable "desired_task_memory" {
  description = "Task Memory Limit"
  default     = 2048
}

variable "domain" {
  description = "Base domain to deploy grafana dashboard access"
  type        = string
}

variable "grafana_port" {
  description = "Port tha grafana should expose"
  type        = number
  default     = 3000
}

variable "is_https" {
  description = "Grafana will be exposed with https?"
  type        = bool
  default     = false
}

variable "launch_type" {
  description = "ECS Service launch type"
  type        = string
  default     = "FARGATE"
  validation {
    condition     = contains(["EC2", "FARGATE"], var.launch_type)
    error_message = "Invalid Input. Valid inputs are EC2 or FARGATE."
  }
}

variable "load_balancer_name" {
  description = "Name of the load balancer to deploy the app"
  type        = string
}

variable "region" {
  description = "Aws region"
  type        = string
  default     = "us-east-1"
}

variable "vpc_id" {
  description = "Id of te vpc to deploy the application"
  type        = string
  default     = ""
}
