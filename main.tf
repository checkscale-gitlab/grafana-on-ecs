provider "aws" {
  region = var.region
}

terraform {
  # backend "s3" {}
}

# Example of configuration
module "grafana" {
  source                     = "git@gitlab.com:eudevops/ecs-service.git?ref=main"
  app_name                   = "grafana"
  app_port                   = var.grafana_port
  cluster_name               = var.cluster_name
  desired_task_cpu           = var.desired_task_cpu
  desired_task_memory        = var.desired_task_memory
  environment_vars           = [{ name = "GF_HTTP_PORT", value = var.grafana_port }]
  filter_subnets_by_tags     = [{ key = "Tier", values = ["Private"] }]
  hc_route                   = "/"
  hc_status_code             = "302"
  iam_policies               = [aws_iam_policy.grafana.arn]
  iam_policies_for_execution = [aws_iam_policy.ecs_ssm_policy.arn]
  image_repository           = "grafana/grafana"
  image_tag                  = "latest"
  is_https                   = var.is_https
  launch_type                = var.launch_type
  load_balancer_name         = var.load_balancer_name
  max_tasks                  = 2
  min_tasks                  = 1
  redirect_rules             = { "host_headers" : [local.grafana_url] }
  secrets                    = local.secrets
  vpc_id                     = data.aws_vpc.selected.id
}
