resource "aws_kms_key" "this" {
  description         = "KMS key for grafana"
  enable_key_rotation = true
  key_usage           = "ENCRYPT_DECRYPT"
}

resource "aws_kms_alias" "this" {
  name          = "alias/grafana"
  target_key_id = aws_kms_key.this.key_id
}
